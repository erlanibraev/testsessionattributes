package kz.factor.tofy.sun.test.service.impl;

import kz.factor.tofy.sun.test.service.IActiveLang;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.SessionAttribute;

import javax.annotation.PostConstruct;

/**
 * Создал Ибраев Ерлан 25.11.16.
 */
@Component
@Scope(value="session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ActiveLang implements IActiveLang {

    protected String DEFAULT_LANG;

    private String activelang;

    @PostConstruct
    public void setUp() {
        if (DEFAULT_LANG == null) {
            DEFAULT_LANG = "ru";
        }
    }


    @Override
    public String getActivelang() {
        return activelang != null ? activelang : DEFAULT_LANG;
    }

    @Override
    public void setActivelang(String activelang) {
        if (activelang != null) {
            this.activelang = activelang;
        }
    }

    @Value("${default.lang}")
    public void setDEFAULT_LANG(String DEFAULT_LANG) {
        this.DEFAULT_LANG = DEFAULT_LANG;
    }

}
