package kz.factor.tofy.sun.test.controllers;

import kz.factor.tofy.sun.test.service.IActiveLang;
import kz.factor.tofy.sun.test.service.impl.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Создал Ибраев Ерлан 25.11.16.
 */
@RestController
public class TestController {

    private IActiveLang activeLang;
    private TestService testService;

    @RequestMapping(value = ""
            , method = RequestMethod.GET
            , produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String test(@RequestParam(name = "activelang", required = false) String activelang) {
        activeLang.setActivelang(activelang);
        return testService.test();
    }

    @Autowired
    public void setActiveLang(IActiveLang activeLang) {
        this.activeLang = activeLang;
    }

    @Autowired
    public void setTestService(TestService testService) {
        this.testService = testService;
    }
}
