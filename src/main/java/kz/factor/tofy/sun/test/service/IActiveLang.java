package kz.factor.tofy.sun.test.service;

/**
 * Создал Ибраев Ерлан 25.11.16.
 */
public interface IActiveLang {
    public String getActivelang();
    public void setActivelang(String activelang);
}
