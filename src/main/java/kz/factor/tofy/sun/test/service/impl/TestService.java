package kz.factor.tofy.sun.test.service.impl;

import kz.factor.tofy.sun.test.service.IActiveLang;
import kz.factor.tofy.sun.test.service.ITestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Создал Ибраев Ерлан 25.11.16.
 */
@Service
public class TestService implements ITestService {

    private IActiveLang activeLang;

    public String test() {
        return activeLang.getActivelang();

    }

    @Autowired
    public void setActiveLang(IActiveLang activeLang) {
        this.activeLang = activeLang;
    }
}
