package kz.factor.tofy.sun.test.controllers;

import kz.factor.tofy.sun.test.service.IActiveLang;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

/**
 * Создал Ибраев Ерлан 25.11.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestControllerTest {
    @Value("${local.server.port}")
    private int port = 0;

    private String url;
    private String urlParam;
    private RestTemplate restTemplate = new RestTemplate();

    @Value("${default.lang}")
    private String defaultLang;

    @Before
    public void setUp() {
        url = "http://localhost:"+port;
        urlParam = url + "?activelang={activelang}";
    }

    @Test
    public void test01() {
        String result = restTemplate.getForObject(url, String.class);
        System.out.println(result);
        assertEquals(result, defaultLang);
    }

    @Test
    public void test02() {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("activelang", "ru");

        String result = restTemplate.getForObject(url, String.class);
        System.out.println(result);
        assertEquals(result, defaultLang);

        result = restTemplate.getForObject(urlParam, String.class, params);
        System.out.println(result);

        assertEquals(result, params.get("activelang"));
    }

    @Test
    public void test03() {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("activelang", "ru");

        HttpEntity<String> result = restTemplate.getForEntity(urlParam, String.class, params);
        result
                .getHeaders()
                .get("Set-Cookie")
                .forEach(System.out::println);
        List<String> cookies = result
                .getHeaders()
                .get("Set-Cookie");


        System.out.println(result.getBody());
        assertEquals(result.getBody(), params.get("activelang"));

        HttpHeaders headers = new HttpHeaders();
        headers.set("Cookie", cookies.stream().collect(Collectors.joining(";")));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        result = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        System.out.println(result.getBody());

        assertEquals(result.getBody(), params.get("activelang"));
    }

}